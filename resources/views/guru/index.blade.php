@extends('templates.default')
@section('content')
	<div class="row">
		<div class="col-md-8 col-md-offset-2">
			<div class="panel panel-default">
				<div class="panel-heading">Form Data Guru</div>
				<div class="panel-body">
					<a href="{{ route('guru.add') }}" class="btn btn-success">Tambah</a>
				</div>
				<table class="table table-hover table-bordered table-striped table-condensed">
					<thead>
						<tr>
							<th>ID</th>
							<th>Nip</th>
							<th>Nama</th>
							<th>Alamat</th>
							<th>Pilihan</th>
						</tr>
					</thead>
					<tbody>
						@foreach ($guru as $val)
						<tr>
							<td>{{ $val->id }}</td>
							<td>{{ $val->nip }}</td>
							<td>{{ $val->nama }}</td>
							<td>{{ $val->alamat }}</td>

							<td>
								<a href="{{ route('guru.edit', $val->id) }}" class="btn btn-warning btn-xs">Ubah</a>
								<a href="" class="btn btn-danger btn-xs">Hapus</a>
							</td>
						</tr>
						@endforeach
					</tbody>
				</table>
			</div>
		</div>
	</div>
@stop