@extends('templates.default')
@section('content')
    <div class="row">
        <div class="col-md-8 col-md-offset-2">
            <div class="panel panel-default">
                <div class="panel-heading">Form Ubah Data Guru</div>
                <div class="panel-body">
                    <form action=" {{ route('guru.patch', $guru->id) }} " method="POST" class="form-horizontal">
                        <input type="hidden" name="_method" value="PATCH">
                        <div class="form-group">
                            <label for="" class="control-label col-md-3">Nis</label>
                            <div class="col-md-6">
                                <input type="text" name="nip" class="form-control" value="{{ Request::old('nip') ?: $guru->nip }}">
                            </div>
                            <div class="col-md-3"></div>
                        </div>
                        <div class="form-group">
                            <label for="" class="control-label col-md-3">Nama</label>
                            <div class="col-md-6">
                                <input type="text" name="nama" class="form-control" value="{{ Request::old('nama') ?: $guru->nama }}">
                            </div>
                            <div class="col-md-3"></div>
                        </div>
                        <div class="form-group">
                            <label for="" class="control-label col-md-3">Alamat</label>
                            <div class="col-md-6">
                                <input type="text" name="alamat" class="form-control" value="{{ Request::old('alamat') ?: $guru->alamat }}">
                            </div>
                            <div class="col-md-3"></div>
                        </div>
                        <div class="col-md-3"></div>
                        <div class="col-md-6">
                            <button type="submit" class="btn btn-primary btn-sm">Simpan</button>
                            <a href=" {{ route('guru') }} " class="btn btn-primary btn-sm">Batal</a>
                        </div>
                        <input type="hidden" name="_token" value="{{ Session::token() }}">
                    </form>
                </div>
            </div>
        </div>
    </div>
@stop